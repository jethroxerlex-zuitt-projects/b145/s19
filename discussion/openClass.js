let students = [];
let sectionStudents = [];

function addStudent(name) {
	//call out the array structure and use the push() to add a new element inside the container.
	students.push(name);
	//display a return in the console
	console.log(name + ' has been added to the list of students');
}
addStudent('Zyrus');
addStudent('John');
addStudent('Marie');
// console.log(students);

function countStudents() {
	//display the number of elements inside the array with the prescribed message.
	console.log('This Class has a total of '+ students.length +' enrolled students');
}

//invocation
countStudents();

function printStudents() {
	//sort the elements inside the array in alphanumeric order.
	students = students.sort();
	// console.log(students);
	//we want to display each element in the console individually.
	students.forEach(function(student){
 		console.log(student);
	})
}

printStudents();

function findStudent(keyword){
	//describe each element inside the array individually.
	let matches = students.filter(function(student){
		//evaluate if each element includes the keyword.
		//convert both the keyword and each element into lowercase.
	return student.toLowerCase().includes(keyword.toLowerCase());

	});
	// console.log(matches) //checker
	//create a control structure that will give the proper response according to the result of the filter.
	if (matches.length === 1) {
	//if there are matches found for the keyword
	console.log(matches[0]+ ' is Enrolled');

	}else if (matches.length > 1) {
		console.log('Multiple Students Matches this keyword');
	}
	 else {
		//no results was found.
		console.log(' No Students matches this keyword');
	}
}
  (findStudent('r'));

  function addSection(section) {
  	//place each student inside the students array inside a new array called sectionStudents and add a section for each element inside the array.
  	//map() -> will create a new array populated with the new elements that passes a condition on a given function/argument.
  	sectionStudents = students.map(function(student){
  		return student + ' is part of section: ' + section;
  	})
  	console.log(sectionStudents);
  }
  addSection('5');

  function removeStudent(name) {
  	//search for the index of John in the array
  	let result = students.indexOf(name);
  	console.log(result);
  	//create a control structure if a result was found
  	if (result >= 0) {
  		//remove the element using splice method.
  		students.splice(result,1)
  		console.log(students);
  	    console.log(name + ' was removed')

  	} else {
  		console.log('No student was Removed');
  	}

  }

  removeStudent('John');
  // console.log(students.indexOf('John')); // this will return the index number of a certain element.