
//Create a location/storage where the friends list will be stored.
let friendsList = []; //non-persistent
console.log(friendsList);

let res = document.getElementById('responseMessage');
let haba = friendsList.length;

//[SECTION] Mutator Functions

	//Create a function to add new friend
	function addNewFriend() {
		//get the data from the user
		let friendName = document.getElementById('friendName').value;
		// alert(friendName);

		//validate the information by creating a control structure that will make sure that the input is NOT empty.
		if (friendName !== '') {
			//proceed with the function.
			//we will use the push() function
			friendsList.push(friendName);
			console.log(friendsList);
			
		} else {
			//failed
			res.innerHTML = '<h5 class="mb-2 mt-5 text-danger">Input is Invalid!</h5>';
		}
	}

//[SECTION] Iteration Functions
	
	//Create function to check how many friends
	function viewListLength() {
		//Asses the length of the arry and inform the user how many elements are there in the array.
		if (haba === 0) {
			//inform the user that the container is empty
			res.innerHTML = 'You Currently have 0 Friends';

		} else {
			//display how many friends
			res.innerHTML = 'You have ' + haba + ' Friends';
		}
	}